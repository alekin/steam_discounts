# -*- coding: utf-8 -*-
from django.apps import AppConfig


class DiscountsConfig(AppConfig):
    name = 'discounts'
    verbose_name = u'Скидки и цены'
