# -*- coding: utf-8 -*-
from django.contrib import admin

from discounts.models import Game


@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    pass
