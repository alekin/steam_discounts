# -*- coding: utf-8 -*-
from discounts import platforms


class Steam(object):
    """Класс для работы со Steam"""

    def __init__(self, game_id):
        self.get_data(game_id)

    def get_data(self, game_id):
        self.data = platforms.Steam(game_id).get_data()

    @property
    def price(self):
        return self.data['price_overview']['final'] / 100

    @property
    def discount(self):
        return self.data['price_overview']['discount_percent']
