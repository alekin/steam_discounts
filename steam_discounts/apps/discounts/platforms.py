# -*- coding: utf-8 -*-
import requests

from django.core.cache import cache


class Steam(object):
    """Класс для работы со Steam"""

    def __init__(self, game_id):
        self.game_id = game_id

    def get_data(self):
        game_id = self.game_id
        cache_key = 'steam_data_game_id_{id}'.format(id=game_id)
        data = cache.get(cache_key)
        if not data:
            r = requests.get('http://store.steampowered.com/api/appdetails/',
                             timeout=2,
                             params={'appids': game_id, 'cc': 'ru', 'l': 'ru'})
            data = r.json()['{id}'.format(id=game_id)]['data']
            cache.set(cache_key, data, 3600)
        return data
