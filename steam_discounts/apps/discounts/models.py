# -*- coding: utf-8 -*-
from django.db import models

from discounts.fields import StoreField


class PriceTracker(models.Model):
    """Модель для отслеживания цен"""

    price = models.IntegerField(
        u'цена')

    date = models.DateField(
        u'дата', auto_now_add=True)

    game = models.ForeignKey(
        'discounts.Game', models.CASCADE,
        verbose_name=u'игра', related_name='trackers')

    class Meta:
        verbose_name = u'трекер'
        verbose_name_plural = u'трекеры'
        ordering = ['date']


class Game(models.Model):
    """Модель игра"""

    title = models.CharField(
        u'название', max_length=100)

    is_dlc = models.BooleanField(
        u'DLC', default=False)

    store = StoreField(
        u'магазин', data_field='store_param', blank=True)

    store_param = models.CharField(
        u'параметр для магазина', max_length=100, blank=True)

    class Meta:
        verbose_name = u'игра'
        verbose_name_plural = u'игры'
        ordering = ['title']
