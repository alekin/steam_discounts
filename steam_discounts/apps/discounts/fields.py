# -*- coding: utf-8 -*-
from django import forms
from django.db import models
from django.utils.module_loading import import_string


class StoreWidget(forms.Select):
    def __init__(self, *args, **kwargs):
        # TODO: Передлать на авторегистрацию модулей магазинов.
        kwargs['choices'] = (
            ('', u'нет'),
            ('Steam', 'Steam'),
        )
        super(StoreWidget, self).__init__(*args, **kwargs)

    def render(self, name, value, attrs=None, choices=()):
        if not isinstance(value, unicode):
            value = value.__class__.__name__
        return super(StoreWidget, self).render(name, value, attrs, choices)


class StoreDescriptor(object):
    def __init__(self, field):
        self.field = field

    def __get__(self, instance, cls=None):
        if instance is None:
            return self

        store = instance.__dict__[self.field.name]
        store_param = instance.__dict__[self.field.data_field]
        if isinstance(store, unicode) and store and store_param:
            value = import_string('discounts.stores.%s' % store)(store_param)
            instance.__dict__[self.field.name] = value

        return instance.__dict__[self.field.name]

    def __set__(self, instance, value):
        if isinstance(value, unicode):
            instance.__dict__[self.field.name] = value


class StoreField(models.Field):
    descriptor_class = StoreDescriptor

    def __init__(self, *args, **kwargs):
        self.data_field = kwargs.pop('data_field')
        kwargs.setdefault('max_length', 200)
        super(StoreField, self).__init__(*args, **kwargs)

    def deconstruct(self):
        # Указываем какие аргументы передать в __init__() чтобы воссоздать его.
        name, path, args, kwargs = super(StoreField, self).deconstruct()
        kwargs['data_field'] = self.data_field
        return name, path, args, kwargs

    def get_internal_type(self):
        # Указываем тип поля в БД.
        return 'CharField'

    def contribute_to_class(self, cls, name, **kwargs):
        # Запускается каждый раз, когда идёт обращение к полю.
        super(StoreField, self).contribute_to_class(cls, name, **kwargs)
        setattr(cls, self.name, self.descriptor_class(self))

    def formfield(self, **kwargs):
        # Определение поля формы для поля модели.
        defaults = {'widget': StoreWidget}
        return super(StoreField, self).formfield(**defaults)

    def get_prep_value(self, value):
        # Преобразовать объект Python обратно в значение для запроса.
        # Так же используется при сохранение значения в БД.
        if value.__class__.__module__ == 'discounts.stores':
            return value.__class__.__name__
        return value

    def value_to_string(self, obj):
        # Указываем как значения сериализуются сериализатором.
        value = self._get_val_from_obj(obj)
        return self.get_prep_value(value)
