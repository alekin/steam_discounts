from steam_discounts.settings.common import *  # NOQA


DEBUG = True

TEMPLATES[0]['OPTIONS']['debug'] = DEBUG

if not TEST:
    INSTALLED_APPS += ('debug_toolbar', )
    MIDDLEWARE_CLASSES += (
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    )


def custom_show_toolbar(request):
    return DEBUG

DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
    'SHOW_TOOLBAR_CALLBACK': 'settings.dev.custom_show_toolbar',
}


if TEST:
    TEMPLATES[0]['OPTIONS']['debug'] = False

TEMPLATES[0]['OPTIONS']['loaders'] = HAML_LOADERS + COMMON_TEMPLATE_LOADERS
