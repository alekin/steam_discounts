import os

from steam_discounts.settings.common import *  # NOQA


DJANGO_CONF = os.environ.get('DJANGO_CONF', 'prod')
if DJANGO_CONF != 'default':
    module = __import__(DJANGO_CONF, globals(), locals(), ['*'])
    for k in dir(module):
        locals()[k] = getattr(module, k)

try:
    from local import *  # NOQA
except ImportError:
    pass
